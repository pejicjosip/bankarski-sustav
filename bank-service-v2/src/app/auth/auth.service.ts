import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'

})



export class AuthService {

  constructor() { }

  public isAuthenticated(): boolean {
    // Check whether the token is expired and return
    // true or false
    //
    return !!localStorage.getItem('access_token')
  }
}

