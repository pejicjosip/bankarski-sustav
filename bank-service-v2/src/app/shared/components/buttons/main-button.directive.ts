import { Directive, ElementRef,HostBinding } from '@angular/core';

@Directive({
  selector: '[main-button]'
})
export class MainButtonDirective {

  @HostBinding('class') get classes(): string {
    return 'mainButton';
  }

}
