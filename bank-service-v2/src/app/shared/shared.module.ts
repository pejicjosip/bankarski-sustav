import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CurrencyFormatPipe } from './components/currency-format.pipe';
import { MainButtonDirective } from './components/buttons/main-button.directive';



@NgModule({
  declarations: [CurrencyFormatPipe, MainButtonDirective],
  imports: [
    CommonModule,

  ],
  exports: [CurrencyFormatPipe, MainButtonDirective]
})
export class SharedModule { }
