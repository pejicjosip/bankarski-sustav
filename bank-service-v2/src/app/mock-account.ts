import { Account } from './account';
import { User } from './user'

export const ACCOUNTS: Account[] = [
      { id: 11, iban:"125478963123", bankName: "Zaba", money: 1250 },
      { id: 12, iban:"125478653123", bankName: "Zaba", money: 1250 },
      { id: 13, iban:"125478963789", bankName: "Zaba", money: 1250 },
      { id: 14, iban:"456478963123", bankName: "Zaba", money: 1250 },
      { id: 15, iban:"125478523123", bankName: "Zaba", money: 1250 },

];

export const USERS: User[] = [
  { email: "user1@gmail.com", password: "user1111"},
  { email: "user2@gmail.com", password: "user2222"},
  { email: "user3@gmail.com", password: "user3333"},
]
