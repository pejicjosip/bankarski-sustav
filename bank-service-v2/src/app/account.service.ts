import { Injectable } from '@angular/core';
import { Account } from './account';
import { ACCOUNTS, USERS } from './mock-account';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { NgControl } from '@angular/forms';
import { User } from './user';




@Injectable({
  providedIn: 'root'
})
export class AccountService {

  private accountUrl = 'api/accounts';
  private userUrl = 'api/users';


  constructor(
    private http: HttpClient,
  ) { }

  getAccount(): Observable<Account[]> {
    return this.http.get<Account[]>(this.accountUrl)

  }

  deleteAccount(id: number): Observable<Account> {
    const url = `${this.accountUrl}/${id}`;
    return this.http.delete<Account>(url)
  }

  updateAccount(account:Account): Observable<Account> {
    const url = `${this.accountUrl}/${account.id}`;
    return this.http.put<Account>(url, account)

  }

  getAccByIban(iban:string) : Observable<Account> {
    const url=`${this.accountUrl}/${iban}`
    return this.http.get<Account>(url)
  }

  getUsers(): Observable <User[]> {
    return this.http.get<User[]>(this.userUrl)
  }


}

