import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import {User} from '../../user';
import { AccountService } from 'src/app/account.service';
import { ACCOUNTS, USERS } from 'src/app/mock-account';
import { Observable, of, Subject, subscribeOn, switchMap } from 'rxjs';
import { Router } from '@angular/router';



@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public loginForm: FormGroup = this.fb.group({});
  public users: User[] = [];
  users$!: Observable<User[]>;

  public token: string = '132456789';

  constructor(private fb: FormBuilder,
              private AccountService: AccountService,
              private router : Router) {

  }

  ngOnInit(): void {
    this.loginForm = this.fb.group({
      email: [null, [Validators.required, Validators.email]],
      password: [null, Validators.required,],
    });

    this.getListOfUsers();
  }


  onSubmit() {

  }

  public getListOfUsers() {
    this.AccountService.getUsers().subscribe(data => {
      this.users = data;
      console.log(this.users);
    }, error => {
      console.log(error)
    });

  }

  logInUser():void{
    console.log("Entered email : ", this.loginForm.value.email)
    console.log("Entered email : ", this.loginForm.value.password)

    let status = this.findUser(this.loginForm.value.email, this.loginForm.value.password);
    if(status===true){
     console.log("log in succesfull")
     window.localStorage.setItem('access_token',this.token)
     this.router.navigate(['home']);
    }else{
      console.log("Login unsuccesfull")
    }
  }

  findUser(email: string, password:string): Boolean{
    let userList = this.users.filter(user => user.email === email && user.password === password);
    if(userList.length == 1){
      return(true)
    }else{
      return(false)
    }
  }

}
function token(arg0: string, token: any) {
  throw new Error('Function not implemented.');
}

