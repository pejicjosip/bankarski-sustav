import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Account } from 'src/app/account';

import { AccountService } from '../../account.service';
import { Observable, of, Subject, subscribeOn, switchMap } from 'rxjs';
import { ACCOUNTS } from 'src/app/mock-account';
import { FormBuilder, FormsModule, FormGroup, FormControl, NgForm, Validators, AbstractControl} from '@angular/forms';
import { LiteralExpr } from '@angular/compiler';


@Component({
  selector: 'app-money-transfer',
  templateUrl: './money-transfer.component.html',
  styleUrls: ['./money-transfer.component.scss']
})
export class MoneyTransferComponent implements OnInit {

  public accounts: Account[] = [];
  public senderAcc: Account = {id: 0, iban: '', bankName: '', money: 0};
  accounts$!: Observable<Account[]>;
  public moneyTransferForm: FormGroup = this.formBuilder.group({});
  constructor(private AccountService: AccountService,
              private formBuilder: FormBuilder,
              private formsModule: FormsModule,) { }

  @Output() closeModalFunc:EventEmitter<any>=new EventEmitter()

  ngOnInit(): void {
    this.getAccount();
    this.moneyTransferForm = this.formBuilder.group({
        sender:[null, Validators.required],
        receiverIban:[null,[Validators.required, Validators.pattern('^[A-Z]{2}(?:[ ]?[0-9]){18,20}$')]],
        cashAmount:[null, [Validators.required, this.minCashValidator]],
    });
  }

  public getAccount() {
    console.log(this.accounts)
    this.AccountService.getAccount().subscribe(data => {
      this.accounts = data;
      console.log(this.accounts);
    }, error=> {
      console.log(error)
    });
  }

  public transferMoney(){
    console.log("transfer money func ")
    let receiverAcc = this.findAcc(this.moneyTransferForm.value.receiverIban);

    console.log("Receiver data: ", receiverAcc)
     this.senderAcc=this.moneyTransferForm.value.sender;

     console.log("sender acc is: ", this.senderAcc)
    if(this.moneyTransferForm.valid){
        if(this.senderAcc.id !== 0 &&
          this.senderAcc.money >= this.moneyTransferForm.value.cashAmount &&
          this.moneyTransferForm.value.cashAmount >= 0){
          //If statements are true take money from sender account
          this.senderAcc.money -= this.moneyTransferForm.value.cashAmount
          this.AccountService.updateAccount(this.senderAcc).subscribe(data => {this.getAccount();},
          error => {
          });
          if(receiverAcc.id !== 0){
            receiverAcc.money += this.moneyTransferForm.value.cashAmount
            this.AccountService.updateAccount(receiverAcc).subscribe(data => {this.getAccount();},
            error => { });
          }
        }else if(parseInt(this.moneyTransferForm.value.cashAmount) <= 0){
          alert("invalid money amount")
        }else if(this.senderAcc.money <= this.moneyTransferForm.value.cashAmount){
          alert("not enough money")
        }

      this.moneyTransferForm.reset();
    }
  }

  minCashValidator(control: AbstractControl): { [key: string]: boolean } | null {

      if (control.value !== undefined && (isNaN(control.value) || control.value <= 0)) {
          return { 'cashMinLimit': true };
      }
      return null;
  }

  findAcc(receiverIban:string): Account{
    let ibanList = this.accounts.filter(account => account.iban === receiverIban);
    console.log(ibanList.length);
    if(ibanList.length == 1){
      console.log(ibanList.length);
      return(ibanList[0])
    }else{
       ibanList[0] = {id: 0, iban: '', bankName: '', money: 0}
        return(ibanList[0])
    }
  }

  closeModal(){
    let data=false;
    this.closeModalFunc.emit(data)
  }

}
