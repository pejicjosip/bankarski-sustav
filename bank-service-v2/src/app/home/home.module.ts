import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home/home.component';
import { AddMoneyComponent } from './add-money/add-money.component';
import { MoneyTransferComponent } from './money-transfer/money-transfer.component';
import { FormsModule, FormControl, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [HomeComponent, AddMoneyComponent, MoneyTransferComponent,],
  imports: [
    CommonModule,
    HomeRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
  ],

})
export class HomeModule { }
