import { Component, OnInit, ElementRef, HostListener, Directive } from '@angular/core';
import { Account } from 'src/app/account';
import { AccountService } from 'src/app/account.service';
import { Observable, of, Subject, subscribeOn, switchMap } from 'rxjs';
import { ACCOUNTS } from 'src/app/mock-account';
import { CashTransferComponent } from 'src/app/cash-transfer/cash-transfer/cash-transfer.component';
import { Router } from '@angular/router';



@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})



export class HomeComponent implements OnInit {

  clicked: string = '';
  public accounts: Account[] = [];
  public selectedAccount: Account = {id: 0, iban: '', bankName: '', money: 0};
  moneyToAdd: string = '';
  public addMoneyDisplayStatus= false;
  public moneyTransferDisplayStatus= false;
  accounts$!: Observable<Account[]>;


  constructor(
    private AccountService: AccountService,
    private elem: ElementRef,
    private router: Router,
    ) { }

  @HostListener('document:click', ['$event.target']) clicked2() {
   this.selectedAccount= {id: 0, iban: '', bankName: '', money: 0};
  }

  ngOnInit(): void {

    this.getAccount();
  }

  public getAccount() {
    console.log(this.accounts)
    this.AccountService.getAccount().subscribe(data => {
      this.accounts = data;
      console.log(this.accounts);
    }, error=> {
      console.log(error)
    });
  }

  public selectAccount(e: Event, account:Account) {
    this.selectedAccount = account;
    console.log(this.selectedAccount);
    e.stopPropagation();
  }


  deleteAcc(): void {

    if(this.selectedAccount.id !== 0) {
      this.AccountService.deleteAccount(this.selectedAccount.id).subscribe(data => {
        this.getAccount();
      }, error => {
      });
    }
    else{
      console.log("no selcted acc")
    }
  }

  public goToPage(pageName:string){
    this.router.navigate([`${pageName}`]);
  }


  public showModal(){

    if(this.selectedAccount.id==0){
      console.log("No selected acc")
    }
    else {
      this.addMoneyDisplayStatus = !this.addMoneyDisplayStatus;

    }
  }

  public showMoneyTransferModal(){
    this.moneyTransferDisplayStatus = !this.moneyTransferDisplayStatus;
  }


  public stopPropagation(e: Event): void {
    e.stopPropagation();
  }


  /* addMoney(): void {
    console.log("dodavanje", this.moneyToAdd)
    this.selectedAccount.money += parseInt(this.moneyToAdd);
    console.log(this.selectedAccount);
    if(this.selectedAccount.id !== 0) {
       this.AccountService.updateAccount(this.selectedAccount).subscribe(data => {
        this.getAccount();
      }, error => {
      });
    }
    else{
      console.log("no selcted acc")
    }

  } */

  status: boolean = false;
  clickEvent(){
      this.status = !this.status;
  }

  public logoutUser(){
    console.log("logout")
    localStorage.clear();
    this.router.navigate(['login'])
  }

  closeModalFunc(data: any){
    console.log(data);
    this.addMoneyDisplayStatus=data;
    this.moneyTransferDisplayStatus=data;
    this.AccountService.getAccount().subscribe(data => {
      this.accounts = data;
      console.log(this.accounts);
    }, error=> {
      console.log(error)
    });
  }

  getMoneyToAdd(data: any){
    console.log(data);
    this.moneyToAdd=data;
    this.selectedAccount.money += parseInt(this.moneyToAdd);
    console.log(this.selectedAccount);
    if(this.selectedAccount.id !== 0) {
       this.AccountService.updateAccount(this.selectedAccount).subscribe(data => {
        this.getAccount();
      }, error => {
      });
    }
    else{
      console.log("no selcted acc")
    }

  }

}






