import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';



@Component({
  selector: 'app-add-money',
  templateUrl: './add-money.component.html',
  styleUrls: ['./add-money.component.scss']
})
export class AddMoneyComponent implements OnInit {

  moneyToAdd: string = '';


  @Input() status: any;
  @Output() closeModalFunc:EventEmitter<any>=new EventEmitter()
  @Output() sendMoneyToAddFunc:EventEmitter<any>=new EventEmitter()
  constructor() { }

  ngOnInit(): void {
  }

  closeModal(){
    let data=false;
    this.closeModalFunc.emit(data)
  }

  sendMoneyToAdd(){
    this.sendMoneyToAddFunc.emit(this.moneyToAdd)
    console.log(this.moneyToAdd)
  }



}
