import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CashTransferRoutingModule } from './cash-transfer-routing.module';
import { CashTransferComponent } from './cash-transfer/cash-transfer.component';
import { FormsModule, FormControl, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    CashTransferComponent
  ],
  imports: [
    CommonModule,
    CashTransferRoutingModule,
    FormsModule,
    ReactiveFormsModule,


  ]
})
export class CashTransferModule { }
