import { Component, OnInit } from '@angular/core';
import { Account } from 'src/app/account';

import { AccountService } from '../../account.service';
import { Observable, of, Subject, subscribeOn, switchMap } from 'rxjs';
import { ACCOUNTS } from 'src/app/mock-account';
import { FormBuilder, FormsModule, FormGroup, FormControl, NgForm, Validators, AbstractControl} from '@angular/forms';
import { LiteralExpr } from '@angular/compiler';




@Component({
  selector: 'app-cash-transfer',
  templateUrl: './cash-transfer.component.html',
  styleUrls: ['./cash-transfer.component.scss']
})
export class CashTransferComponent implements OnInit {

  public accounts: Account[] = [];
  public senderAcc: Account = {id: 0, iban: '', bankName: '', money: 0};
  // public receiverAcc: Account = {id: 0, iban: '', bankName: '', money: 0};

  accounts$!: Observable<Account[]>;

  public cashTransferForm: FormGroup = this.formBuilder.group({});


  constructor(private AccountService: AccountService,
              private formBuilder: FormBuilder,
              private formsModule: FormsModule,) { }



  ngOnInit(): void {

    this.getAccount();
    this.cashTransferForm = this.formBuilder.group({
        sender:[null, Validators.required],
        receiverIban:[null, [Validators.required, Validators.pattern('^[A-Z]{2}(?:[ ]?[0-9]){18,20}$')]],
        cashAmount:[null, [Validators.required, this.minCashValidator] ],
    });




  }
  //this.cashTransferForm.valid;


  public getAccount() {
    console.log(this.accounts)
    this.AccountService.getAccount().subscribe(data => {
      this.accounts = data;
      console.log(this.accounts);
    }, error=> {
      console.log(error)
    });
  }

  public transferMoney(){
    let receiverAcc = this.findAcc(this.cashTransferForm.value.receiverIban);
    console.log("reciver acc is: ", receiverAcc)
    console.log("Receiver data: ", receiverAcc)
     this.senderAcc=this.cashTransferForm.value.sender;
    if(this.cashTransferForm.valid){
        if(this.senderAcc.id !== 0 &&
          this.senderAcc.money >= this.cashTransferForm.value.cashAmount &&
          this.cashTransferForm.value.cashAmount >= 0){
          //If statements are true take money from sender account
          this.senderAcc.money -= this.cashTransferForm.value.cashAmount
          this.AccountService.updateAccount(this.senderAcc).subscribe(data => {this.getAccount();},
          error => {
          });
          if(receiverAcc.id !== 0){
            receiverAcc.money += this.cashTransferForm.value.cashAmount
            this.AccountService.updateAccount(receiverAcc).subscribe(data => {this.getAccount();},
            error => { });
          }
        }else if(parseInt(this.cashTransferForm.value.cashAmount) <= 0){
          alert("invalid money amount")
        }else if(this.senderAcc.money <= this.cashTransferForm.value.cashAmount){
          alert("not enough money")
        }

      this.cashTransferForm.reset();
    }
  }

  minCashValidator(control: AbstractControl): { [key: string]: boolean } | null {

      if (control.value !== undefined && (isNaN(control.value) || control.value <= 0)) {
          return { 'cashMinLimit': true };
      }
      return null;
  }

  findAcc(receiverIban:string): Account{
    let ibanList = this.accounts.filter(account => account.iban === receiverIban);
    if(ibanList.length == 1){
      console.log(ibanList.length);
      return(ibanList[0])
    }else{
       ibanList[0] = {id: 0, iban: '', bankName: '', money: 0}
        console.log("Error with iban database")
        return(ibanList[0])
    }
  }



}



