
export class Account {
  constructor(
    public id: number,
    public iban: string,
    public bankName: string,
    public money: number,
  ){}
}
