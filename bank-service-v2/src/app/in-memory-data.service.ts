import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api';
import { Account } from './account';

@Injectable({
  providedIn: 'root'
})
export class InMemoryDataService implements InMemoryDbService {

  createDb() {
    const accounts = [
      { id: 11, iban:"AB121234123412341234", bankName: "Zaba", money: 1250 },
      { id: 12, iban:"825478653123", bankName: "PBZ", money: 62621 },
      { id: 13, iban:"125478963789", bankName: "Adiko Bank", money: 26630 },
      { id: 14, iban:"456478963123", bankName: "Hippo", money: 165225 },
      { id: 15, iban:"125478523123", bankName: "Zagrebačka Banka", money: 962232 },


    ];

    const users = [
      { email: "user1@gmail.com", password: "user1111"},
      { email: "user2@gmail.com", password: "user2222"},
      { email: "user3@gmail.com", password: "user3333"},
      ];
      return {accounts, users}
    }
  }


